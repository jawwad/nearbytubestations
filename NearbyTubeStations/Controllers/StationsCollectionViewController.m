//
//  StationsCollectionViewController.m
//  NearbyTubeStations
//
//  Created by Jawwad Ahmad on 5/2/17.
//  Copyright © 2017 Jawwad Ahmad. All rights reserved.
//

#import "StationsCollectionViewController.h"
#import "FacilityNameCell.h"
#import "LeftAlignedCollectionViewFlowLayout.h"
#import "SectionHeaderView.h"
#import "TubeStation.h"
#import "TubeArrivalCell.h"
#import "TubeArrival.h"
#import "OHHTTPStubs.h"
#import "OHPathHelpers.h"
#import "SVProgressHUD.h"

@import CoreLocation;

NSInteger const kNumberOfArrivalsToShow = 3;
NSInteger const kRefreshInterval = 30;
NSInteger const kSearchRadiusMeters = 1000;
CGFloat const kHorizontalMargins = 10;
CGFloat const kHeaderCellHeight = 44;
NSInteger const kDebugDelayTime = 0; // Set to test activity indicators
CLLocationCoordinate2D const cityCenterCoordinate = {51.5007, -0.1246}; // Coordinate for Big Ben


@interface StationsCollectionViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, CLLocationManagerDelegate>

@property (nonatomic) NSArray<TubeStation *> *tubeStations;
@property (nonatomic) CLLocationManager *locationManager;
@property (nonatomic) CLLocation *locationToUse;
@property (nonatomic) NSTimer *refreshTimer;

@end


@implementation StationsCollectionViewController


/// Call this method to use stubs for the HTTP responses
- (void)stubRequests
{
    [OHHTTPStubs stubRequestsPassingTest:^BOOL(NSURLRequest * _Nonnull request) {
        return [request.URL.absoluteString hasPrefix:@"https://api.tfl.gov.uk/StopPoint"];
    } withStubResponse:^OHHTTPStubsResponse * _Nonnull(NSURLRequest * _Nonnull request) {
        NSString *path = request.URL.path;
        NSString *fileName = [path containsString:@"Arrivals"] ? @"HTTPStubs/arrivals.json" : @"HTTPStubs/stop_points.json";
        NSString *filePath = OHPathForFile(fileName, self.class);
        return [[[OHHTTPStubsResponse alloc] initWithFileAtPath:filePath statusCode:200 headers:nil] responseTime:kDebugDelayTime];
    }];
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];

    self.refreshTimer = [NSTimer scheduledTimerWithTimeInterval:10 repeats:YES block:^(NSTimer * _Nonnull timer) {
        [self.collectionView reloadData];
    }];

    // Uncomment to load data from fixtures for development
    // [self stubRequests];

    // Configure the flow layout
    LeftAlignedCollectionViewFlowLayout *flowLayout = (id)self.collectionView.collectionViewLayout;
    flowLayout.sectionInset = UIEdgeInsetsMake(6, kHorizontalMargins, 20, kHorizontalMargins);
    flowLayout.headerReferenceSize = CGSizeMake(320, kHeaderCellHeight); // Needed when using a custom flow layout, thought these numbers don't seem to matter
    flowLayout.minimumInteritemSpacing = 1;
    flowLayout.minimumLineSpacing = 1;

    // Register the SectionHeaderView
    UINib *nib = [UINib nibWithNibName:@"SectionHeaderView" bundle:nil];
    [self.collectionView registerNib:nib forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"SectionHeaderView"];

    // Triggers locationManager:didChangeAuthorizationStatus:
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
}


// MARK: - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status
{
    switch ([CLLocationManager authorizationStatus]) {
        case kCLAuthorizationStatusNotDetermined:
            [self.locationManager requestWhenInUseAuthorization];
            return;
        case kCLAuthorizationStatusAuthorizedWhenInUse:
        case kCLAuthorizationStatusAuthorizedAlways:
            [SVProgressHUD showWithStatus:@"Determining Location..."];
            [self.locationManager requestLocation];
            return;
        case kCLAuthorizationStatusDenied:
        case kCLAuthorizationStatusRestricted:
            NSLog(@"❌ Error: App cannot proceed due to lack of location authorization");
            return;
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    CLLocation *myRealLocation = [locations lastObject];
    CLLocation *cityCenterLocation = [[CLLocation alloc] initWithLatitude:cityCenterCoordinate.latitude longitude:cityCenterCoordinate.longitude];
    CLLocationDistance distanceFromLondonCenter = [myRealLocation distanceFromLocation:cityCenterLocation];

    // If 20KM away from london city center force use the hardcoded cityCenterLocation
    self.locationToUse = (distanceFromLondonCenter < 20000) ? myRealLocation : cityCenterLocation;

    [self fetchData];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(nonnull NSError *)error
{
    NSLog(@"❌ locationManager:didFailWithError: %@", error.localizedDescription);
}

- (void)fetchData
{
    [SVProgressHUD showWithStatus:@"Fetching Tube Stations..."];

    // Construct the endpoint with NSURLComponents
    NSURLComponents *components = [[NSURLComponents alloc] initWithString:@"https://api.tfl.gov.uk/StopPoint"];
    NSURLQueryItem *stopTypesItem = [NSURLQueryItem queryItemWithName:@"stoptypes" value:@"NaptanMetroStation"]; // NaptanMetroStation == Tube Station
    NSURLQueryItem *latItem = [NSURLQueryItem queryItemWithName:@"lat" value:[NSString stringWithFormat:@"%g", self.locationToUse.coordinate.latitude]];
    NSURLQueryItem *lonItem = [NSURLQueryItem queryItemWithName:@"lon" value:[NSString stringWithFormat:@"%g", self.locationToUse.coordinate.longitude]];
    NSURLQueryItem *radiusItem = [NSURLQueryItem queryItemWithName:@"radius" value:[NSString stringWithFormat:@"%ld", kSearchRadiusMeters]];
    NSURLQueryItem *appIdItem = [NSURLQueryItem queryItemWithName:@"app_id" value:@"ad7347e8"];
    NSURLQueryItem *appKeyItem = [NSURLQueryItem queryItemWithName:@"app_key" value:@"0e2c98f42ba0117731732ec7b492c0c7"];
    components.queryItems = @[stopTypesItem, latItem, lonItem, radiusItem, appIdItem, appKeyItem];


    [[[NSURLSession sharedSession] dataTaskWithURL:[components URL] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"❌ HTTP Response Error: %@", error.localizedDescription);
        } else {
            NSError *jsonSerializationError = nil;
            NSDictionary *stoptypesJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&jsonSerializationError];

            if (jsonSerializationError != nil) {
                NSLog(@"❌ JSON Serialization Error: %@", error.localizedDescription);
            }

            NSArray *stopPoints = stoptypesJson[@"stopPoints"];

            // Construct TubeStation objects from the stopPoints array
            NSMutableArray *tubeStations = [NSMutableArray array];

            for (NSDictionary *stopPoint in stopPoints) {
                NSString *name = stopPoint[@"commonName"];
                NSString *identifier = stopPoint[@"stationNaptan"];

                // Retrieve facilities by inspecting additionalProperties
                NSArray *additionalProperties = stopPoint[@"additionalProperties"];

                NSMutableArray *facilities = [NSMutableArray array];
                for (NSDictionary *propertyDict in additionalProperties) {
                    if ([propertyDict[@"category"] isEqualToString:@"Facility"]) {
                        [facilities addObject:propertyDict[@"key"]];
                    }
                }

                // Initialize a tubeStation object
                NSArray *sortedFacilities = [[facilities copy] sortedArrayUsingSelector:@selector(compare:)];
                TubeStation *tubeStation = [[TubeStation alloc] initWithName:name identifier:identifier facilities:sortedFacilities];
                [tubeStations addObject:tubeStation];
            }

            self.tubeStations = [tubeStations copy];

            // The custom flow layout needs a reference to the tubeStations to determine what cells are for facilities
            LeftAlignedCollectionViewFlowLayout *flowLayout = (id)self.collectionView.collectionViewLayout;
            flowLayout.tubeStations = self.tubeStations;

            // Load the nearest Tube Stations
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.collectionView reloadData];
            });

            // Now fetch the arrival times for the individual stations
            [SVProgressHUD showWithStatus:@"Fetching Tube Times..."];

            for (TubeStation *tubeStation in tubeStations) {
                // Update the existing components property
                components.path = [NSString stringWithFormat:@"/StopPoint/%@/Arrivals", tubeStation.identifier];
                components.queryItems = @[appIdItem, appKeyItem];

                [[[NSURLSession sharedSession] dataTaskWithURL:[components URL] completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
                    if (error != nil) {
                        NSLog(@"❌ Error: %@", error.localizedDescription);
                    } else {
                        NSArray *arrivalJson = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];

                        if (jsonSerializationError != nil) {
                            NSLog(@"❌ JSON Serialization Error: %@", error.localizedDescription);
                        }

                        // Initialize TubeArrival objects
                        NSMutableArray *tubeArrivals = [NSMutableArray array];

                        for (NSDictionary *arrivalDict in arrivalJson) {
                            NSString *platformName = arrivalDict[@"platformName"];
                            NSString *arrivalTimeString = arrivalDict[@"expectedArrival"];

                            NSDate *arrivalTime = [[[NSISO8601DateFormatter alloc] init] dateFromString:arrivalTimeString];

                            // Uncomment for testing with stubs
                            // arrivalTime = [[NSDate date] dateByAddingTimeInterval:[arrivalJson indexOfObject:arrivalDict] * 60];
                            TubeArrival *arrival = [[TubeArrival alloc] initWithName:platformName arrivalTime:arrivalTime];
                            [tubeArrivals addObject:arrival];
                        }

                        // Sort arrivals by arrival time
                        NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"expectedArrivalTime" ascending:YES];
                        tubeStation.arrivals = [tubeArrivals sortedArrayUsingDescriptors:@[descriptor]];

                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.collectionView reloadData];
                            [SVProgressHUD dismiss]; // Ok to dismiss after first response since only the first station's arrivals fit on a view (along with the 2nd station's facilities)
                        });
                    }
                }] resume];
            }
        }
    }] resume];
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *indexPath = sender;
    TubeStation *tubeStation = self.tubeStations[indexPath.section];
    segue.destinationViewController.title = tubeStation.facilities[indexPath.item];
}



// MARK: - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return self.tubeStations.count;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    TubeStation *tubeStation = self.tubeStations[section];
    return tubeStation.facilities.count + MIN(kNumberOfArrivalsToShow, tubeStation.futureArrivals.count);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    TubeStation *tubeStation = self.tubeStations[indexPath.section];

    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        SectionHeaderView *sectionHeader = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"SectionHeaderView" forIndexPath:indexPath];
        sectionHeader.label.text = tubeStation.name;
        return sectionHeader;
    } else {
        NSAssert(false, @"Should never happen");
        return nil;
    }
}


- (NSDateFormatter *)timeFormatter
{
    static NSDateFormatter *_dateFormatter;
    if (_dateFormatter == nil) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        _dateFormatter.dateStyle = NSDateFormatterNoStyle;
        _dateFormatter.timeStyle = NSDateFormatterShortStyle;
    }

    return _dateFormatter;
}


// MARK: - UICollectionViewDelegate

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TubeStation *tubeStation = self.tubeStations[indexPath.section];

    if ([self isFacilitiesCell:indexPath]) {
        FacilityNameCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FacilityNameCell" forIndexPath:indexPath];
        cell.label.text = tubeStation.facilities[indexPath.item];
        return cell;
    } else {
        NSInteger arrivalIndex = indexPath.item - tubeStation.facilities.count;

        TubeArrival *tubeArrival = tubeStation.futureArrivals[arrivalIndex];
        TubeArrivalCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TubeArrivalCell" forIndexPath:indexPath];
        cell.nameLabel.text = tubeArrival.name;
        cell.arrivalTimeLabel.text = [[self timeFormatter] stringFromDate:tubeArrival.expectedArrivalTime];
        return cell;
    }
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Only Facilities cells are tappable
    if ([self isFacilitiesCell:indexPath]) {
        [self performSegueWithIdentifier:@"FacilityDetailSegue" sender:indexPath];
    }
}

- (BOOL)isFacilitiesCell:(NSIndexPath *)indexPath
{
    TubeStation *tubeStation = self.tubeStations[indexPath.section];
    return indexPath.row < tubeStation.facilities.count;
}

// MARK: - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TubeStation *tubeStation = self.tubeStations[indexPath.section];

    if ([self isFacilitiesCell:indexPath]) {
        NSString *facilityName = tubeStation.facilities[indexPath.row];

        NSDictionary *attributes = @{NSFontAttributeName : [FacilityNameCell font]};
        CGSize size = [facilityName sizeWithAttributes:attributes];
        CGFloat horizontalPadding = 26;
        CGFloat verticalPadding = 16;

        return CGSizeMake(size.width + horizontalPadding, size.height + verticalPadding);
    } else {
        UIEdgeInsets sectionInset = ((UICollectionViewFlowLayout *)collectionView.collectionViewLayout).sectionInset;
        return CGSizeMake(collectionView.bounds.size.width - sectionInset.left - sectionInset.right, kHeaderCellHeight);
    }
}


@end
