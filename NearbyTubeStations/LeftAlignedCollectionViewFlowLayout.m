//
//  LeftAlignedCollectionViewFlowLayout.m
//  NearbyTubeStations
//
//  Created by Jawwad Ahmad on 5/2/17.
//  Copyright © 2017 Jawwad Ahmad. All rights reserved.
//

#import "LeftAlignedCollectionViewFlowLayout.h"

@implementation LeftAlignedCollectionViewFlowLayout

- (NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    NSArray *attributes = [super layoutAttributesForElementsInRect:rect];

    NSMutableArray *updatedAttributes = [NSMutableArray array];

    CGFloat leftMargin = self.sectionInset.left;
    CGFloat maxY = -1.0f;

    for (UICollectionViewLayoutAttributes *attribute in attributes) {
        UICollectionViewLayoutAttributes *updatedAttribute = [attribute copy];
        [updatedAttributes addObject:updatedAttribute];

        TubeStation *tubeStation = self.tubeStations[attribute.indexPath.section];

        if (updatedAttribute.representedElementKind == UICollectionElementKindSectionHeader) {
            // do nothing, header will already be in the correct place
        } else if (updatedAttribute.indexPath.row >= tubeStation.facilities.count) {
            // This is a Tube Arrival Cell, do nothing as well
            CGFloat furtherInset = 3;
            updatedAttribute.frame = CGRectMake(updatedAttribute.frame.origin.x + furtherInset, updatedAttribute.frame.origin.y, updatedAttribute.frame.size.width - (furtherInset * 2), updatedAttribute.frame.size.height);
        } else {
            // Facilities Cell
            if (updatedAttribute.frame.origin.y >= maxY) {
                leftMargin = self.sectionInset.left;
            }

            updatedAttribute.frame = CGRectMake(leftMargin, updatedAttribute.frame.origin.y, updatedAttribute.frame.size.width, updatedAttribute.frame.size.height);

            leftMargin += updatedAttribute.frame.size.width + self.minimumInteritemSpacing;
            maxY = MAX(CGRectGetMaxY(updatedAttribute.frame), maxY);
        }
    }

    return updatedAttributes;
}

@end
