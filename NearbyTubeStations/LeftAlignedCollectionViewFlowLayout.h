//
//  LeftAlignedCollectionViewFlowLayout.h
//  NearbyTubeStations
//
//  Created by Jawwad Ahmad on 5/2/17.
//  Copyright © 2017 Jawwad Ahmad. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TubeStation.h"

@interface LeftAlignedCollectionViewFlowLayout : UICollectionViewFlowLayout

@property (nonatomic) NSArray<TubeStation *> *tubeStations;

@end
