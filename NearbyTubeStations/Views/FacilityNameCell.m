//
//  FacilityNameCell.m
//  NearbyTubeStations
//
//  Created by Jawwad Ahmad on 5/2/17.
//  Copyright © 2017 Jawwad Ahmad. All rights reserved.
//

#import "FacilityNameCell.h"

@interface FacilityNameCell()

@property (nonatomic) UIColor *originalBackgroundColor;
@property (nonatomic) UIColor *selectedBackgroundColor;

@end


@implementation FacilityNameCell

+ (UIFont *)font
{
    return [UIFont systemFontOfSize: 17];
}

- (void)awakeFromNib
{
    [super awakeFromNib];

    self.originalBackgroundColor = self.label.backgroundColor;
    self.selectedBackgroundColor = [UIColor brownColor];
    self.label.font = [FacilityNameCell font];
    self.label.highlightedTextColor = [UIColor whiteColor];
    self.label.layer.cornerRadius = 5;
    self.label.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];

    self.label.backgroundColor = selected ? self.selectedBackgroundColor : self.originalBackgroundColor;
}

- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];

    self.label.backgroundColor = highlighted ? self.selectedBackgroundColor : self.originalBackgroundColor;
}

@end
