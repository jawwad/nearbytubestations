//
//  SectionHeaderView.h
//  NearbyTubeStations
//
//  Created by Jawwad Ahmad on 5/2/17.
//  Copyright © 2017 Jawwad Ahmad. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SectionHeaderView : UICollectionReusableView

@property (weak, nonatomic) IBOutlet UILabel *label;

@end
