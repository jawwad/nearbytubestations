//
//  TubeStation.h
//  NearbyTubeStations
//
//  Created by Jawwad Ahmad on 5/2/17.
//  Copyright © 2017 Jawwad Ahmad. All rights reserved.
//

#import <Foundation/Foundation.h>

@class TubeArrival;

@interface TubeStation : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *identifier;
@property (nonatomic) NSArray<NSString *> *facilities;
@property (nonatomic) NSArray<TubeArrival *> *arrivals;

@property (nonatomic, readonly) NSArray<TubeArrival *> *futureArrivals;

- (instancetype)initWithName:(NSString *)name identifier:(NSString *)identifier facilities:(NSArray<NSString *> *)facilities;

@end
