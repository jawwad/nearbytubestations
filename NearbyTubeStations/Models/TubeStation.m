//
//  TubeStation.m
//  NearbyTubeStations
//
//  Created by Jawwad Ahmad on 5/2/17.
//  Copyright © 2017 Jawwad Ahmad. All rights reserved.
//

#import "TubeStation.h"

@implementation TubeStation

- (instancetype)initWithName:(NSString *)name identifier:(NSString *)identifier facilities:(NSArray<NSString *> *)facilities
{
    self = [super init];
    if (self) {
        _name = name;
        _identifier = identifier;
        _facilities = [facilities copy];
    }
    return self;
}

- (NSArray<TubeArrival *> *)futureArrivals
{
    NSPredicate *futureDatePredicate = [NSPredicate predicateWithFormat:@"expectedArrivalTime > %@", [NSDate date]];
    return [self.arrivals filteredArrayUsingPredicate:futureDatePredicate];
}

@end
