//
//  TubeArrival.m
//  NearbyTubeStations
//
//  Created by Jawwad Ahmad on 5/2/17.
//  Copyright © 2017 Jawwad Ahmad. All rights reserved.
//

#import "TubeArrival.h"

@implementation TubeArrival

- (instancetype)initWithName:(NSString *)name arrivalTime:(NSDate *)arrivalTime
{
    self = [super init];
    if (self) {
        _name = name;
        _expectedArrivalTime = arrivalTime;
    }
    return self;
}

@end
