//
//  TubeArrival.h
//  NearbyTubeStations
//
//  Created by Jawwad Ahmad on 5/2/17.
//  Copyright © 2017 Jawwad Ahmad. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TubeArrival : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic) NSDate *expectedArrivalTime;

- (instancetype)initWithName:(NSString *)name arrivalTime:(NSDate *)arrivalTime;

@end
