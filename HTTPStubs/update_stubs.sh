#!/usr/bin/env sh

curl -s "https://api.tfl.gov.uk/StopPoint/940GZZLUWSM/Arrivals?app_id=ad7347e8&app_key=0e2c98f42ba0117731732ec7b492c0c7" | python -mjson.tool > arrivals.json

## This doesn't really need to be updated that frequently so its commented out
curl -s "https://api.tfl.gov.uk/Stoppoint?lat=51.5007&lon=-0.1246&stoptypes=NaptanMetroStation&radius=800&app_id=ad7347e8&app_key=0e2c98f42ba0117731732ec7b492c0c7" | python -mjson.tool > stop_points.json
